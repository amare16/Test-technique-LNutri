const { readFileSync } = require('fs');
const path = require('path');
const jsonFilePath = path.join(__dirname, '../dataFromJson/allFruits.json')
let loadFruits = () => JSON.parse(readFileSync(jsonFilePath));

module.exports = { loadFruits };