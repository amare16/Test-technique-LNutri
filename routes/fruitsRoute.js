const express = require('express');
const router = express.Router();

const getAllFruits = require('../controllers/Fruits');

router.get('/fruits', getAllFruits);

module.exports = router;