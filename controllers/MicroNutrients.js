const { loadMicroNutrients } = require('../models/microNutrientsModel');

const getAllMicroNutrients = (req, res) => {
    res.send(loadMicroNutrients());
};

module.exports = getAllMicroNutrients;